<div align="center">

   <h1>
      <a href="">
         <img src="https://gitlab.com/BDAg/intersheep/-/wikis/uploads/eefb3237f9c3596e751f85f22adf1e71/Group_2.png">
      </a>
   </h1>

   Destinado aos alunos do curso de Big Data no Agronegócio, o INTERSHEEP é uma aplicação web que tem como objetivo ajudar alunos do curso a encontrar vagas para o cumprimento do estágio obrigatório.
</div>

## 💻 Team
- [Fernanda Maria Pereira Miranda - Scrum Master](https://gitlab.com/FernandaM.P.M)
- [César Augusto Matos Ladeira - Product Owner](https://gitlab.com/ladeiraA)
- [Pedro Henrique Tonon Ferreira - Developer](https://gitlab.com/pehetofe)

## ✨ Funcionalidades

- Coleta de vagas de estágio na área do curso
- Envia mensagens pelo WhatsApp

## 🔨 Tecnologias

As seguintes ferramentas foram usadas:

- [Visual Studio Code](https://code.visualstudio.com/)
- [Python](https://www.python.org/)
- [Selenium](https://www.selenium.dev/)

## 📦 Montagem do Ambiente do Desenvolvimento:

#### Instalando Python:

```sh
Baixar Instalador no site https://www.python.org/downloads/

```

#### Instalando Bibliotecas:

-Requests

```sh
python -m pip install requests

```

-Beautifulsoup4

```sh
pip install beautifulsoup4

```

-LXML

```sh
pip install lxml

```

-Selenium

```sh
pip install -U selenium

`````

#### Instalando Visual Studio Code:

```sh
https://code.visualstudio.com/download

```

### Clonar Projeto

```sh
git clone git@gitlab.com:BDAg/intersheep.git
```



