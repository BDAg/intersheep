import requests
from bs4 import BeautifulSoup
import csv

site = "https://www.infojobs.com.br/vagas-de-estagiario-ti.aspx?Palabra=Est%c3%a1gio&idw=2"

req = requests.get(site)
soup = BeautifulSoup(req.content,'html.parser')

LIST_link = []
LIST_empresa = []
LIST_data = []
LIST_area = []

###########LINK#############
cont = 0
for link in soup.find_all('a',{'class' : 'vagaTitle js_vacancyTitle'}):
    #print(f"{cont}º - {link.get('href')}")
    LIST_link.insert(cont, str(link.get('href')))
    cont += 1
############################

###########Empresa##########
cont = 0
for empresa in soup.find_all('div',{'class' : 'vaga-company js_linkCompany'}):  
    empresa = empresa.text
    #print(f"{cont}º - {empresa.strip()}")
    LIST_empresa.insert(cont, str(empresa.strip()))
    cont += 1
#############################

############Data#############
cont = 0
for data in soup.find_all('span',{'class' : 'data'}):
    #print(f"{cont}º - {', '.join([s.strip() for s in data])}")
    data = str(', '.join([s.strip() for s in data]))
    if data != 'Mais de um mes':
        dataMod = data.replace(" ", " às ")
    else:
        dataMod = data
    LIST_data.insert(cont, dataMod)
    cont +=1
#############################

#############Area############
cont = 0   
for Area in soup.find_all('p',{'class' : 'area'}):
    Area = str(Area.get('title'))
    AreaS = Area.replace(" ","")
    Area = AreaS.replace("-"," - ")
    #print(Area)
    LIST_area.insert(cont, str(Area))
    cont +=1
#############################

########Local W.I.P #########
'''cont = 0   
for local in soup.find_all('p',{'class' : 'location2'}):
    local = str(local.get('title'))
    print(local)
    cont +=1'''
#############################

vagas = csv.writer(open("vagas.csv", "w+"))
for i in range(len(LIST_empresa)):
    vagas.writerow((LIST_empresa[i],LIST_area[i],LIST_data[i],LIST_link[i]))
print("data stored in 'vagas.csv'")
