from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
import time
import csv

### COLETA OS DADOS DO ARQUIVO .CSV ###
LIST_empresa = []
LIST_area = []
LIST_data = []
LIST_link = []
for elemento in csv.reader(open("vagas.csv", "r")):
    if elemento != []:
        LIST_empresa.append(elemento[0])
        LIST_area.append(elemento[1])
        LIST_data.append(elemento[2])
        LIST_link.append(elemento[3])
print("collected data = ",LIST_empresa,LIST_area,LIST_data,LIST_link)

### CONTATOS QUE RECEBERÃO AS MENSAGENS ###
contatos = ['CONTATO']
posicaoVaga = 1

### EMOJIS E MENSAGENS DEFINIDAS ###
Ovelha = ":sheep"
Oculos = ':sunglasses'
Grafico = ':bar chart'
pc = ':pc '

### ABRE O GOOGLE COM A EXTENSÃO INSTALADA ###
driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get('https://web.whatsapp.com/')
time.sleep(15) #TEMPO DE ESPERA PARA O QR CODE SER LIDO

### FUNÇÃO: BUSCA CONTATOS ###
def buscar_contato(contato):
    campo_pesquisa = driver.find_element_by_xpath('//div[contains(@class,"copyable-text selectable-text")]')
    time.sleep(2)
    campo_pesquisa.click()
    campo_pesquisa.send_keys(contato)
    campo_pesquisa.send_keys(Keys.ENTER)

### FUNÇÃO: ESCREVE E ENVIA AS MENSAGENS ###
def enviar_mensagem():
    campo_mensagem = driver.find_elements_by_xpath('//div[contains(@class,"copyable-text selectable-text")]')
    campo_mensagem[1].click()
    time.sleep(2)
    #######################################
    campo_mensagem[1].send_keys(str(Ovelha))
    campo_mensagem[1].send_keys(Keys.ENTER)
    campo_mensagem[1].send_keys(str(" *INTERSHEEP BOT* "))
    #campo_mensagem[1].send_keys(str(Grafico))
    #time.sleep(2)
    #campo_mensagem[1].send_keys(Keys.ENTER)
    campo_mensagem[1].send_keys(str(pc))
    time.sleep(1)
    campo_mensagem[1].send_keys(Keys.ENTER)
    time.sleep(2)
    campo_mensagem[1].send_keys(Keys.CONTROL ,Keys.ENTER)
    campo_mensagem[1].send_keys(Keys.CONTROL ,Keys.ENTER)
    ########################################
    campo_mensagem[1].send_keys(str(f"A empresa *{LIST_empresa[posicaoVaga]}* está oferecendo vagas de estagio para: {LIST_area[posicaoVaga]}"))
    campo_mensagem[1].send_keys(Keys.CONTROL ,Keys.ENTER)
    campo_mensagem[1].send_keys(Keys.CONTROL ,Keys.ENTER)
    campo_mensagem[1].send_keys(str("Segue o link para mais informações: "))
    campo_mensagem[1].send_keys(Keys.CONTROL ,Keys.ENTER)
    campo_mensagem[1].send_keys(str(LIST_link[posicaoVaga]))
    campo_mensagem[1].send_keys(Keys.CONTROL ,Keys.ENTER)
    campo_mensagem[1].send_keys(Keys.CONTROL ,Keys.ENTER)
    campo_mensagem[1].send_keys(str(f"Esta vaga foi postada {LIST_data[posicaoVaga]}"))
    time.sleep(3)
    campo_mensagem[1].send_keys(Keys.ENTER)

### FUNÇÃO: ENVIAR MÍDIA ###
def enviar_midia(midia):
    driver.find_element_by_css_selector("span[data-icon='clip']").click()
    attach = driver.find_element_by_css_selector("input[type='file']")
    attach.send_keys(midia)
    time.sleep(3)
    send = driver.find_element_by_xpath("//div[contains(@class, '_3Git-')]")
    send.click()    

### EXECUTA AS DEF PARA TODOS OS CONTATOS ###
for contato in contatos:
    buscar_contato(contato)
    enviar_mensagem()        
    time.sleep(1)
